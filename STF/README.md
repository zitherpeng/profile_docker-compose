# STF ( Smartphone Test Farm ) For OrayTest 

**地址: [http://stf.oraytest.com](http://stf.oraytest.com)**
## PREVIEW
![](https://img.oraytest.com/zither/doc/20190616_171321.png)  

![](https://img.oraytest.com/zither/doc/20190616_171336.png)
## DEPLOYMENT
### Server ( master ) 部署
#### Step 1
```bash
# 下拉本 repository，进入 repository 目录 
git clone git@gitlab.oraytest.com:zitherpeng/profile_docker-compose.git 
cd profile_docker-compose

# 复制 nginx 配置到的 nginx 配置目录，并使其生效
mv STF/stf.oraytest.com.conf /usr/local/nginx/conf/conf.d/
nginx -t && nginx -s reload
```

#### Step 2
```bash
# 部署 STF 主节点（ web 服务端 ）
cd /STF/server
source .env
docker-compose up -d --build  # 启动预先编排好的容器集 
```  

e.g.
```bash
[root@centos7 stf_server]# docker-compose up -d --build
Creating stf_server_rethinkdb_1 ... done
Creating stf_server_stf-migrate_1 ... done
Creating stf_server_stf-app_1     ... done
Creating stf_server_stf-auth_1    ... done
Creating stf_server_stf-websocket_1 ... done
Creating stf_server_stf-api_1       ... done
Creating stf_server_stf-storage-plugin-apk_1 ... done
Creating stf_server_stf-storage-plugin-image_1 ... done
Creating stf_server_stf-storage-temp_1         ... done
Creating stf_server_stf-triproxy-app_1         ... done
Creating stf_server_stf-processor_1            ... done
Creating stf_server_stf-triproxy-dev_1         ... done
Creating stf_server_stf-reaper_1               ... done
Creating stf_server_stf-log-rethinkdb_1        ... done
```
 

### Client ( provider ) 部署

#### Ubuntu

##### step 1
```bash
# 安装ADB
apt -y install adb

# 编辑 adbd.service
vim /etc/systemd/system/adbd.service

[Unit]
Description=Android Debug Bridge daemon
After=network.target
[Service]
Restart=always
RestartSec=2s
Type=forking
User=root
ExecStartPre=/usr/bin/adb kill-server
ExecStart=/usr/bin/adb start-server
ExecStop=/usr/bin/adb kill-server

[Install]
WantedBy=multi-user.target
```  
启动 ADB
```bash
# 开机自启动
systemctl enable adbd.service
# 启动
systemctl start adbd.service
```

##### step 2
启动容器
```
git clone git@gitlab.oraytest.com:zitherpeng/profile_docker-compose.git 
cd profile_docker-compose/STF/client

source .env
docker-compose up -d --build  
```

#### MacOS
macos 不支持 docker 内搭建 provider 节点，即使容器启动，也不能识别插入的安卓手机，参考 STF 作者原话：https://github.com/sorccu/docker-adb/issues/8


macos 需要直接安装 stf，使用 stf 命令启动：
##### step 1
```bash
# 安装相关依赖
brew install rethinkdb graphicsmagick zeromq protobuf yasm pkg-config

# stf 基于 node
npm i stf@3.4.0 -g
```

##### step 2
```bash
# 加载环境变量
source .env

# 启动 stf provider
stf provider --name "${STF_PROVIDER}" --connect-sub tcp://devside.$STF_HOST:7250 --connect-push tcp://devside.$STF_HOST:7270 --storage-url $STF_URL --public-ip $STF_PROVIDER.$STF_HOST --min-port=15000 --max-port=25000 --heartbeat-interval 20000 --screen-ws-url-pattern "ws://${STF_HOST}/d/${STF_PROVIDER}/<%= serial %>/<%= publicPort %>/"
```



